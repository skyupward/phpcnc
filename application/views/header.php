<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="PHPCNC">
    <title><?php echo $title; ?> | PHPCNC</title>
    <base href="<?php echo base_url(); ?>" />
    <link rel="shortcut icon" href="static/img/site/favicon.ico" type="image/vnd.microsoft.icon">
    <link rel="icon" href="static/img/site/favicon.ico" type="image/vnd.microsoft.icon">
    <link href="http://cdn.staticfile.org/twitter-bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="static/css/layout.css" rel="stylesheet" type="text/css">
